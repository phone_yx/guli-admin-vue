import request from '@/utils/request'


export default {
  //根据id查章节小节
  getAllChapterVideo(courseId) {
    return request({
      url: `/eduservice/chapter/getChapterVideo/${courseId}`,
      method: 'get'
    })
  },
  //查询小节
  getChapterById(chapterId) {
    return request({
      url: `/eduservice/chapter/getChapter/${chapterId}`,
      method: 'get'
    })
  },
  //修改小节
  updateChapter(eduChapter) {
    return request({
      url: `/eduservice/chapter/updateChapter/${eduChapter.id}`,
      method: 'put',
      data: eduChapter
    })
  },
  //添加小节
  addChapter(eduChapter) {
    return request({
      url: `/eduservice/chapter/addChapter`,
      method: 'post',
      data: eduChapter
    })
  },
  //删除小节
  deleteChapter(chapterId){
    return request({
      url: `/eduservice/chapter/deleteChapter/${chapterId}`,
      method: 'delete'
    })
  }


}
