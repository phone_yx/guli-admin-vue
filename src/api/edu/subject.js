import request from '@/utils/request'
/**
 * 课程分类接口
 */
export default {
  //课程分类列表
  getSubjectList() {
    return request({
      url: `/eduservice/subject/getAllSubject`,
      method: 'get',
    })
  }

}
