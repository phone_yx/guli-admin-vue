import request from '@/utils/request'

export default {

  //查询小节
  getVideo(videoId) {
    return request({
      url: `/eduservice/video/getVideo/${videoId}`,
      method: 'get'
    })
  },
  //增加小节
  addVideo(eduVideo) {
    return request({
      url: `/eduservice/video/addVideo`,
      method: 'post',
      data: eduVideo
    })
  },
  //删除小节
  deleteVideoById(id) {
    return request({
      url: `/eduservice/video/${id}`,
      method: 'delete'
    })
  },
  //修改小节
  updateVideo(eduVideo) {
    return request({
      url: `/eduservice/video/updateVideo/${eduVideo.id}`,
      method: 'put',
      data: eduVideo
    })
  },
  //删除阿里云视频方法
  deleteAlyVideoById(id) {
    return request({
      url: `/eduvod/alyvideo/deleteAlyVideo/${id}`,
      method: 'delete'
    })
  }


}
