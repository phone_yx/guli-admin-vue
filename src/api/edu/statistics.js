import request from '@/utils/request'

export default {
  //生成图标数据
  createStatistics(date){
    return request({
      url:`/edustatistics/daily/registerCount/${date}`,
      method:'post'
    })
  },
  //展示图标数据
  showDataStatistics(searchObj){
    return request({
      url:`/edustatistics/daily/getTableData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
      method:'get'
    })
  }




}
