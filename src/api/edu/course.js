import request from '@/utils/request'

/**
 * 课程接口
 */
export default {
  //添加课程信息
  addCourseInfo(courseInfo) {
    return request({
      url: `/eduservice/course/addCourseInfo`,
      method: 'post',
      data: courseInfo
    })
  },
  //查询所有讲师
  getAllTeachers() {
    return request({
      url: `/eduservice/teacher/getAll`,
      method: 'get'
    })
  },
  //通过课程id查询课程信息
  getCourseInfoById(courseId) {
    return request({
      url: `/eduservice/course/getCourseInfo/${courseId}`,
      method: 'get'
    })
  },
  //修改课程信息
  updateCourseInfo(courseInfo) {
    return request({
      url: `/eduservice/course/updateCourseInfo`,
      method: 'post',
      data: courseInfo
    })
  },
  //查询最终发布的课程vo对象
  getCoursePublishInfo(courseId) {
    return request({
      url: `/eduservice/course/getCoursePublish/${courseId}`,
      method: 'get'
    })
  },
  //修改课程最终发布状态
  updateCourseStatus(courseId) {
    return request({
      url: `/eduservice/course/updateCourseStatus/${courseId}`,
      method: 'post'
    })
  },
  //根据id删除课程
  deleteCourseById(courseId) {
    return request({
      url: `/eduservice/course/deleteCourse/${courseId}`,
      method: 'delete'
    })
  },
  //课程条件查询分页
  coursePage(current,size,courseQuery){
    return request({
      url:`/eduservice/course/conditionCoursePage/${current}/${size}`,
      method: 'post',
      data: courseQuery
    })
  }

}
