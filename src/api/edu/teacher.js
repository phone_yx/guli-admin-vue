import request from '@/utils/request'
/**
 * 教师接口
 */
export default {
  //教师列表（条件查询分页）
  getTeacherListPage(current, size, teacherQuery) {
    return request({
      url: `/eduservice/teacher/teacherPageCondition/${current}/${size}`,
      method: 'post',
      //表示把对象转换json传递到接口里面
      data: teacherQuery
    })
  },
  //删除讲师
  deleteById(id) {
    return request({
      url: `/eduservice/teacher/${id}`,
      method: "delete",
    })
  },
  addTeacher(teacher) {
    return request({
      url: '/eduservice/teacher/addTeacher',
      method: "post",
      data: teacher
    })
  },
  getTeacherInfo(id) {
    return request({
      url: `/eduservice/teacher/getTeacher/${id}`,
      method: 'get'
    })
  },
  updateTeacher(teacher){
    return request({
      url:`/eduservice/teacher/${teacher.id}`,
      method: 'put',
      data: teacher
    })
  }
}
